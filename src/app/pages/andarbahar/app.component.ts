import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Router } from '@angular/router';
import { ConstantService } from 'src/app/services/constant.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-andarbahar',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss', '../shared.scss']
})

export class AndarbaharComponent implements OnInit {
  state = "new";
  selected = "false";
  htmlCollection: any = '';
  cycleTimer: any = '';
  itemsId: any = '';
  sectionDeg: any = '';
  radianSectionDeg: any = '';
  radiusLength: any = 200;
  rotation: any = 0;
  center: any = '';
  
  isBatPlacedOpen: boolean = false;
  isBatPlacedTimeStop: boolean = false;
  isJokerCardActive: boolean = false;

  //
  

  // @Input() betState = null;

  GameWinner = "JOKER";

  player = "Bet for";
  odds = null;

  timeLeft: number = 60;
  interval: any;

  $: any;


  /* #region  mute un mute */
  newCounterLength: number = 60;
  newCounterInterval: any;
  public audio: HTMLAudioElement;
  isMute: boolean = true;
  /* #endregion mute un mute */


  startTimer() {
    let count = 0;
    this.interval = setInterval(() => {
      if (this.timeLeft > 0) {
        this.timeLeft--;
        count = count + 4.18;
        document.getElementById("progress").setAttribute('stroke-dasharray', count + ', 251.2');
      } else {
        this.selected = "false";
        this.state = "new";
        const buttons = document.querySelectorAll('.button');
        buttons.forEach(el => el.classList.remove("active"));
        this.timeLeft = 60;
      }

      // Sequence
      if (this.timeLeft <= 60) {
        this.state = "new";
        document.getElementById("content").classList.remove("winner");
      }
      if (this.timeLeft <= 50) {
        this.state = "bet";
        document.getElementById("content").classList.remove("winner");
      }
      if (this.timeLeft <= 20) {
        this.state = "open";
        document.getElementById("content").classList.add("winner");
      }
    }, 1000)
  }

  constructor(
    public constant: ConstantService,
    public router: Router
  ) { }

  GameList: any;
  CurrentGame: string;
  GameCode: string;
  GameStatus: number = 1;

  quickBet = false;
  receiveMessage($event) {
    this.quickBet = $event
  }

  AddBet(player, odds) {
    if (this.GameStatus === 1) {
      if (this.quickBet) {
        this.player = player;
        this.odds = odds;
        new window.bootstrap.Toast(document.getElementById('success')).show();
      } else {
        this.player = player;
        this.odds = odds;
      }
    } else {
      // this.toastr.error('Betting closed. You can\'t bet right now.');
    }
  }

  toasts() {
    var x = document.getElementById("snackbar");
    x.className = "show";
    setTimeout(function () { x.className = x.className.replace("show", ""); }, 5000);
  }

  public muteAudio(isMute: any): void {
    this.isMute = isMute
  }
  ngOnInit() {

    // this.turnRight();

    /* #region sound mute unmute  */
    this.audio = new Audio();
    this.audio.src = "../../../assets/audio/beep.mp3";
    this.audio.load();
    //this.audio.autoplay = true;
    this.audio.play();

    this.newCounterInterval = setInterval(() => {
      if (this.newCounterLength != 0) {
        this.newCounterLength = this.newCounterLength - 1;

        if (this.isMute) { this.audio.pause(); } else { this.audio.play(); }

        if (this.newCounterLength == 0) {
          this.isBatPlacedOpen = this.isBatPlacedTimeStop = false;
        }

      }

    }, 1000);
    /* #endregion sound mute unmute */


    this.GameList = this.constant.gameList;
    this.GameCode = this.router.url.replace('/', '');
    this.CurrentGame = this.GameList[this.GameCode].name;

    this.startTimer();

    const buttons = document.querySelectorAll('.button');

    buttons.forEach(el => el.addEventListener('click', event => {
      buttons.forEach(el => el.classList.remove("active"));
      el.classList.add("active");
      this.selected = "true";
    }));


    setTimeout(() => {
    this.htmlCollection = document.getElementsByClassName('item') as HTMLCollectionOf<HTMLElement>;
    this.itemsId = Array.from(this.htmlCollection);
    // console.log(this.itemsId);


    this.sectionDeg = 360 / this.itemsId.length;
    this.radianSectionDeg = this.sectionDeg * Math.PI * 2 / 360;
    this.radiusLength = 165;
    for (var i = 0; i < this.itemsId.length; i++) {
      this.itemsId[i].style.top = this.radiusLength * Math.sin(this.radianSectionDeg * i - 1.5708) - 50 + 'px';
      this.itemsId[i].style.left = this.radiusLength * Math.cos(this.radianSectionDeg * i - 1.5708) - 50 + 'px';
    }
    this.rotation = 0;
    this.center = document.getElementById('center');
    
    this.turnRight();
    this.turnLeft();  
    }, 500);

    this.createTimer(this.newCounterLength);
  }

  createTimer(time) {
    var countrObj: any = {};
    countrObj.counter = document.getElementById('cycleTimer');
    countrObj.counter = countrObj.counter.getContext('2d');
    countrObj.no = time;
    countrObj.pointToFill = 4.72;
    countrObj.cw = countrObj.counter.canvas.width;
    countrObj.ch = countrObj.counter.canvas.height;
    countrObj.diff;
    
    var fill = setInterval(() => this.fillCounter(countrObj, time, fill), 1000);
  }

  fillCounter(countrObj, time, fill) {
    countrObj.diff = ((countrObj.no / time) * Math.PI * 2 * 10);
    countrObj.counter.clearRect(0, 0, countrObj.cw, countrObj.ch);
    countrObj.counter.lineWidth = 4;
    countrObj.counter.strokeStyle = '#E800EB';
    countrObj.counter.beginPath();
    countrObj.counter.arc(110, 110, 70, countrObj.pointToFill, countrObj.diff / 10 + countrObj.pointToFill);
    countrObj.counter.stroke();

    if (countrObj.no == 0) {
        clearTimeout(fill);
    }
    countrObj.no--;
}

  // Slider JS
  turnLeft() {
    this.rotation = this.rotation + this.radianSectionDeg;
    this.center.style.transform = 'rotate(' + this.rotation + 'rad) scale(0.79)';
    for (var i = 0; i < this.itemsId.length; i++) {
      this.itemsId[i].style.transform = 'rotate(' + -this.rotation + 'rad) scale(0.79)';
    }
  }

  turnRight() {
    this.rotation = this.rotation - this.radianSectionDeg;
    this.center.style.transform = 'rotate(' + this.rotation + 'rad) scale(0.79)';
    for (var i = 0; i < this.itemsId.length; i++) {
      this.itemsId[i].style.transform = 'rotate(' + -this.rotation + 'rad) scale(0.79)';
    }
  }

  ngOnDestroy() {
    clearInterval(this.interval);
  }

  directClick() {
    // console.log("clicked");

  }

  //1
  //9
  // rotateCoins(itemPosition){
  //   // console.log('itemPosition',itemPosition);
  //   // console.log('coinsAry',this.coinsAry);
  //   //this.turnRight()
  //   //this.turnRight()
  //   if(itemPosition!==0){
  //     let newAry = []
  //     if(itemPosition == 1){
  //       for(let i=0; i<this.coinsAry.length; i++){
  //         if(i==this.coinsAry.length-1){
  //           newAry.push(this.coinsAry[0])
  //         }else{
  //           newAry.push(this.coinsAry[i+1])
  //         }
  //       }
  //       this.coinsAry = newAry;
  //       this.turnRight()
  //     }else if(itemPosition == 2){
  //       for(let i=0; i<this.coinsAry.length; i++){
  //         if(i>=this.coinsAry.length-2){
  //           if(i==this.coinsAry.length-2){
  //             newAry.push(this.coinsAry[0])  
  //           }else{
  //             newAry.push(this.coinsAry[1])  
  //           }
  //         }else{
  //           newAry.push(this.coinsAry[i+2])
  //         }
  //       }
  //       this.coinsAry = newAry;
  //       this.turnRight()
  //       this.turnRight()
  //     }else if(itemPosition == 8){
  //       for(let i=0; i<this.coinsAry.length; i++){
  //         if(i==0){
  //           newAry.push(this.coinsAry[this.coinsAry.length-2])
  //         }else if(i==1){
  //           newAry.push(this.coinsAry[this.coinsAry.length-1])
  //         }else{
  //           newAry.push(this.coinsAry[i-2])
  //         }
  //       }
  //       this.coinsAry = newAry;
  //       this.turnLeft()
  //       this.turnLeft()
  //     }else if(itemPosition == 9){
  //       for(let i=0; i<this.coinsAry.length; i++){
  //         if(i==0){
  //           newAry.push(this.coinsAry[this.coinsAry.length-1])
  //         }else{
  //           newAry.push(this.coinsAry[i-1])
  //         }
  //       }
  //       this.coinsAry = newAry;
  //       this.turnLeft()
  //     }
  //   }
  // }
  rotateCoins(itemPosition){
    console.log('itemPosition',itemPosition);
    console.log('coinsAry',this.coinsAry);
    //this.turnRight()
    //this.turnRight()
    if(itemPosition!==0){
      let newAry = []
      if(itemPosition == 1){
        for(let i=0; i<this.coinsAry.length; i++){
          if(i==this.coinsAry.length-1){
            newAry.push(this.coinsAry[0])
          }else{
            newAry.push(this.coinsAry[i+1])
          }
        }
        this.coinsAry = newAry;
        this.turnRight()
      }else if(itemPosition == 2){
        for(let i=0; i<this.coinsAry.length; i++){
          if(i>=this.coinsAry.length-2){
            if(i==this.coinsAry.length-2){
              newAry.push(this.coinsAry[0])
            }else{
              newAry.push(this.coinsAry[1])
            }
          }else{
            newAry.push(this.coinsAry[i+2])
          }
        }
        this.coinsAry = newAry;
        this.turnRight()
        this.turnRight()
      }else if(itemPosition == 3){
        for(let i=0; i<this.coinsAry.length; i++){
          if(i>=this.coinsAry.length-3){
            if(i==this.coinsAry.length-3){
              newAry.push(this.coinsAry[0])
            }else if(i==this.coinsAry.length-2){
              newAry.push(this.coinsAry[1])
            }else{
              newAry.push(this.coinsAry[2])
            }
          }else{
            newAry.push(this.coinsAry[i+3])
          }
        }
        this.coinsAry = newAry;
        this.turnRight()
        this.turnRight()
        this.turnRight()
      }else if(itemPosition == 11){
        for(let i=0; i<this.coinsAry.length; i++){
          if(i==0){
            newAry.push(this.coinsAry[this.coinsAry.length-3])
          }else if(i==1){
            newAry.push(this.coinsAry[this.coinsAry.length-2])
          }else if(i==2){
            newAry.push(this.coinsAry[this.coinsAry.length-1])
          }else{
            newAry.push(this.coinsAry[i-3])
          }
        }
        this.coinsAry = newAry;
        this.turnLeft()
        this.turnLeft()
        this.turnLeft()
      }else if(itemPosition == 12){
        for(let i=0; i<this.coinsAry.length; i++){
          if(i==0){
            newAry.push(this.coinsAry[this.coinsAry.length-2])
          }else if(i==1){
            newAry.push(this.coinsAry[this.coinsAry.length-1])
          }else{
            newAry.push(this.coinsAry[i-2])
          }
        }
        this.coinsAry = newAry;
        this.turnLeft()
        this.turnLeft()
      }else if(itemPosition == 13){
        for(let i=0; i<this.coinsAry.length; i++){
          if(i==0){
            newAry.push(this.coinsAry[this.coinsAry.length-1])
          }else{
            newAry.push(this.coinsAry[i-1])
          }
        }
        this.coinsAry = newAry;
        this.turnLeft()
      }
    }
  }
  coinsAry:any = [1000, 1010, 1020, 103, 104, 105 ,106, 107, 108, 109, 110, 111, 1120, 1130];
  // coinsAry:any = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13];

  toggleIsBatPlacedOpen(){


    this.isBatPlacedOpen = true;
    this.isBatPlacedTimeStop = false;
    this.isJokerCardActive = false;
    setTimeout(() => {
      this.isBatPlacedOpen = false;
      this.isBatPlacedTimeStop = true;

      setTimeout(() => {
        this.isJokerCardActive = true;

        this.andarCardLoop();
        this.baharCardLoop();
      }, 1000);

    }, 4000);
  }

  andarCardLoop(i = 0) {
    const andarCards = document.getElementById('andar-cards').getElementsByClassName('flip-card');
    setTimeout(() => {
      andarCards[i].className += ' ' + 'active';
      i++;
      
      if (i < andarCards.length) {
        this.andarCardLoop(i);
      }
    }, 1000);
  }

  baharCardLoop(i = 0) {
    const baharCards = document.getElementById('bahar-cards').getElementsByClassName('flip-card');
    setTimeout(() => {
      baharCards[i].className += ' ' + 'active';
      i++;
      
      if (i < baharCards.length) {
        this.baharCardLoop(i);
      }
    }, 1000);
  }

}
