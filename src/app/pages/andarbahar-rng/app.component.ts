import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Router } from '@angular/router';
import { ConstantService } from 'src/app/services/constant.service';
import { Subscription } from 'rxjs';



@Component({
  selector: 'app-andarbahar-rng',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss', '../shared.scss']
})

export class AndarbaharComponentRNG implements OnInit {
  state = "new";
  selected = "false";
  // @Input() betState = null;

  GameWinner = "JOKER";

  player = "Bet for";
  odds = null;

  timeLeft: number = 60;
  intervalsss: any = 1000;
  interval: any = 1000;
  counter: any = 60;
  value: any;






  startTimer() {
    let count = 0;
    this.interval = setInterval(() => {
      if (this.timeLeft > 0) {
        this.timeLeft--;
        count = count + 4.18;
        document.getElementById("progress").setAttribute('stroke-dasharray', count + ', 251.2');
        // document.querySelector(".rotateBlend").style.transform = "rotate(0deg)");
        document.getElementById("rotateBlend").classList.add("rotate-anim");
      } else {
        this.selected = "false";
        this.state = "new";
        const buttons = document.querySelectorAll('.button');
        buttons.forEach(el => el.classList.remove("active"));
        this.timeLeft = 60;
        document.getElementById("rotateBlend").classList.remove("rotate-anim");
      }

      // Sequence
      if (this.timeLeft <= 60) {
        this.state = "new";
        document.getElementById("content").classList.remove("winner");
      }
      if (this.timeLeft <= 50) {
        this.state = "bet";
        document.getElementById("content").classList.remove("winner");
      }
      if (this.timeLeft <= 20) {
        this.state = "open";
        document.getElementById("content").classList.add("winner");
      }
    }, 1000)
  }

  constructor(
    public constant: ConstantService,
    public router: Router
  ) {



    // var audio = new Audio();
    // audio.src = "../../../assets/audio/beep.mp3";
    // audio.load();
    // audio.play();
  }

  GameList: any;
  CurrentGame: string;
  GameCode: string;
  GameStatus: number = 1;



  /* #region  mute un mute */
  newCounterLength: number = 60;
  newCounterInterval: any;
  public audio: HTMLAudioElement;
  isMute: boolean = true;
  /* #endregion mute un mute */

  ngOnInit() {

    /* #region sound mute unmute  */
    this.audio = new Audio();
    this.audio.src = "../../../assets/audio/beep.mp3";
    this.audio.load();
    //this.audio.autoplay = true;
    this.audio.play();

    this.newCounterInterval = setInterval(() => {
      if (this.newCounterLength != 0) {
        this.newCounterLength--;

        if (this.isMute) { this.audio.pause(); } else { this.audio.play(); }

      }

    }, 1000);
    /* #endregion sound mute unmute */


    this.GameList = this.constant.gameList;
    this.GameCode = this.router.url.replace('/', '');
    this.CurrentGame = this.GameList[this.GameCode].name;

    this.startTimer();

    const buttons = document.querySelectorAll('.button');

    buttons.forEach(el => el.addEventListener('click', event => {
      buttons.forEach(el => el.classList.remove("active"));
      el.classList.add("active");
      this.selected = "true";
    }));

  }


  public muteAudio(isMute: any): void {
    this.isMute = isMute
  }


  quickBet = false;
  receiveMessage($event) {
    this.quickBet = $event
  }

  AddBet(player, odds) {
    if (this.GameStatus === 1) {
      if (this.quickBet) {
        this.player = player;
        this.odds = odds;
        new window.bootstrap.Toast(document.getElementById('success')).show();
      } else {
        this.player = player;
        this.odds = odds;
      }
    } else {
      // this.toastr.error('Betting closed. You can\'t bet right now.');
    }
  }



  ngOnDestroy() {
    clearInterval(this.interval);
    clearInterval(this.newCounterInterval);
  }
}
