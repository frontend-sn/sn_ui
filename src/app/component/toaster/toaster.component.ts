import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-toaster',
  templateUrl: './toaster.component.html',
  styleUrls: ['./toaster.component.scss']
})
export class ToasterComponent implements OnInit {

  constructor() { }

  toasts(id) {
    var x = document.getElementById(id);
    x.classList.add("show");
    setTimeout(function () { x.className = x.className.replace("show", ""); }, 5000);
  }

  ngOnInit(): void {
  }

}
