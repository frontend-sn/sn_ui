import { Component, OnInit, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-action-tray',
  templateUrl: './action-tray.component.html',
  styleUrls: ['./action-tray.component.scss'],
})
export class ActionTrayComponent implements OnInit {
  constructor() {}

  @Output() onMute = new EventEmitter<any>();

  ngOnInit(): void {
    // var link = document.getElementById('action-tray-ptya')
    // link.click()

    setTimeout(() => {
      //document.getElementById('sound-mute-off-on').click()
      // var element = document.getElementById('sound-mute-off-on');
      // if(document.createEvent)
      // {
      //   element.dispatchEvent(new Event('mousedown'));
      //   element.dispatchEvent(new Event('mouseup'));
      // }
    }, 1000);

    // actionTry
  }
  
  coinControls: boolean = false;
  onclickaddclass(e, popId) {
    // document.querySelector(".ro-bg.active").classList.remove('active');
    document.getElementById(e).classList.add('active');

    document.getElementById(e).parentElement.classList.add('active');
    // e.classList.add('active');
    
    document.getElementById(popId).classList.add('show');
    // document.getElementById(popId).style.display = 'unset';
    // document.getElementById(popId).style.cssText = `display: block; transform: scale(1);`;
    
    document.addEventListener('click', (evt) => {
      this.coinControls = !this.coinControls;
      console.log('evt.target', evt.target);
      let id = (<HTMLInputElement>evt.target).getAttribute('id');
      
      if (id == popId || id == popId+'Close') {
        document.getElementById(popId).classList.remove('show');
        // document.getElementById(popId).style.cssText = `display: none; transform: scale(0);`;
        // document.getElementById(popId).style.display = 'unset';
        document.getElementById(e).parentElement.classList.remove('active');
        document.getElementById(e).classList.remove('active');
        document.removeEventListener;
      }
    });
  }
  onclickremoveclass(e, btnid) {
    document.getElementById(e).classList.remove('active');
    document.getElementById(btnid).classList.remove('active');
  }

  activeDiv: boolean = true;

  isMute: boolean = true;
  soundControl() {
    console.log('sound conte');

    this.isMute = !this.isMute;
    this.onMute.emit(this.isMute);

    // var link = document.getElementById('action-tray-ptya')
    // link.click()
    // link.click()

    // var alrm = document.getElementById('ssalarm')

    //let audioPlayer = <HTMLVideoElement> document.getElementById('ssalarm');
    //audioPlayer.play();
    //if(this.isMute){
    //audioPlayer.volume = 0
    //}else{
    //audioPlayer.volume = 1

    //}
  }

  inforMation: boolean = true;
  inforMationClick() {
    this.inforMation = !this.inforMation;
  }

  touchHand: boolean = true;
  touchHandClick() {
    this.touchHand = !this.touchHand;
  }
}
