import { Directive, Input, Output, EventEmitter, OnChanges, OnDestroy } from '@angular/core';

import { Subject, Observable, Subscription, timer } from 'rxjs';
import { switchMap, take, tap } from 'rxjs/operators';



@Directive({
  selector: '[counter]'
})
export class CounterDirective implements OnChanges, OnDestroy {

  private _counterSource$ = new Subject<any>();
  private _subscription = Subscription.EMPTY;

  @Input() counter: number;
  @Input() interval: number;
  @Input() isMute: boolean;
  @Output() value = new EventEmitter<number>();



  audio = new Audio();


  constructor() {

    this._subscription = this._counterSource$.pipe(
      switchMap(({ interval, count }) =>

        timer(0, interval).pipe(
          take(count),
          tap(() => {
            this.value.emit(--count)
            this.playAudio()
          })
        )
      )
    ).subscribe();
  }

  playAudio(): void {

    //this.audio.play();

  }

  ngOnInit() {
     //this.audio.src = "../../../assets/audio/beep.mp3";

    this.audio.load();

  }


  ngOnChanges() {

    // if(this.isMute){
    //   this.audio.volume = 0;
    // }else{
    //   this.audio.volume = 1;
    // }

    this._counterSource$.next({ count: this.counter, interval: this.interval });
  }

  ngOnDestroy() {
    this._subscription.unsubscribe();
  }

}
